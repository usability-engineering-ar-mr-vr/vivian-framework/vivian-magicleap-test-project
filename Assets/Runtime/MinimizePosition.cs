using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimizePosition : MonoBehaviour
{
    public GameObject ObjectToFollow;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (ObjectToFollow != null)
        {
            float newY = Mathf.Min(this.transform.position.y, ObjectToFollow.transform.position.y);
            if (newY < this.transform.position.y)
            {
                this.transform.position = new Vector3(this.transform.position.x, newY, this.transform.position.z);
            }
        }
    }
}
